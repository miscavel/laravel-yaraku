<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
	protected $fillable = ['author_id', 'title'];

	/**
	 * Used to add author_name column to the model
	 */
	public function assignAuthorName()
    {
        $this->author_name = $this->author->name;
    }
	
    public function author()
    {
    	return $this->belongsTo(Author::class);
    }

    /**
     * Used to return columns that are viewable in tables and forms (for BookController:create() and
     * BookController:edit())
     */
    public function editableAttributes()
    {
        return $this->only(['title']);
    }

    /**
     * Used to return columns that are viewable in tables and forms (for BookController:index() and
     * BookController:show())
     */
    public function showableAttributes()
    {
        $this->assignAuthorName();
        return $this->only(['id', 'author_name', 'title']);
    }
}
