<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use DB;
use Illuminate\Validation\Rule;
use App\Exports\BooksExport;
use Maatwebsite\Excel\Facades\Excel;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const DIRECTORY = 'books';

    /**
     * Used as an option to decide which columns to export
     */
    const EXPORT_COLUMNS_KEY = [
        '0' => ['title'],
        '1' => ['title', 'author_name']
    ];

    public function index(Request $request)
    {
        $books = $this->getFilteredResult($request);
        return view(self::DIRECTORY . '.index', [
            'items' => $books,

            /**
             *
             * Option is used to give additional column for view / edit / delete
             * For the 1st option, it translates to a HEAD request to : /books/{{ $book->id }}
             * For the 2nd option, it translates to a HEAD request to : /books/{{ $book->id }}/edit
             * For the 3rd option, it translates to a DELETE request to : /books//{{ $book->id }}
             *
             */

            'options' => [
                ['name' => 'View', 'icon' => 'fa-eye', 'method_form' => 'HEAD', 'method' => '', 'action_prefix' => '/' . self::DIRECTORY, 'action_suffix' => '', 'type' => 'link'],
                ['name' => 'Edit', 'icon' => 'fa-pencil', 'method_form' => 'HEAD', 'method' => '', 'action_prefix' => '/' . self::DIRECTORY, 'action_suffix' => '/edit', 'type' => 'link'],
                ['name' => 'Delete', 'icon' => 'fa-trash', 'method_form' => 'POST', 'method' => method_field('DELETE'), 'action_prefix' => '/' . self::DIRECTORY, 'action_suffix' => '', 'type' => 'button']
            ],

            /**
             *
             * Filter array is used for the filter.blade.template
             * search = contains an array of searchable columns
             * sort = contains an array of sortable columns
             * url = the url of index() for search GET requests and create button redirect
             *
             */

            'filter' => [
                'search' => ['author', 'book'],
                'sort' => ['author', 'book'],
                'url' => '/' . self::DIRECTORY
            ]
        ]);
    }


    /**
     * Get a list of books based on the search parameters
     */
    public function getFilteredResult(Request $request)
    {
        $books = Book::where('id', '>', 0);
        if ($request->search_field === 'author')
        {

            /**
             * If search by author, first fetch a list of authors containing the given name
             */

            $apiRequest = Request::create('/authors/fetch/' . $request->search_value, 'GET');
            $response = \Route::dispatch($apiRequest);
            $json = json_decode($response->getOriginalContent());
            $authors = [];
            foreach ($json as $item)
            {
                array_push($authors, $item->id);
            }

            /**
             * Then, get a list of books that contain those authors' author_id
             */

            $books = Book::whereIn('author_id', $authors);
        }
        else if ($request->search_field === 'book')
        {

            /**
             * If search by book, then look for books which title contains the given string
             */

            $books = Book::where(DB::raw('LOWER(title)'), 'LIKE', '%' . strtolower($request->search_value) . '%');
        }

        if ($request->action === 'asc' || $request->action === 'desc')
        {

            /**
             * Sort the current search result based on the given column, 'authors.name' or 'books.title'
             * $request->action determines the direction of sorting, 'asc' or 'desc'
             */

            $orderColumn = ($request->sort_field === 'author') ? 'authors.name' : 'books.title';
            $books->join('authors', 'authors.id', '=', 'books.author_id')
                ->orderBy($orderColumn, $request->action)
                ->select('books.*');
        }
        return $books->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::DIRECTORY . '.create', [
            'title' => 'Create Book', //Form title
            'items' => Book::make()->editableAttributes(), //Fetch editable items from empty Book Model
            'readonly' => false, //Input fields are editable
            'method_form' => 'POST', //Upon pressing confirm, send a POST request to $confirmAction
            'method' => '', //No custom method is required
            'confirmAction' => '/' . self::DIRECTORY, //Upon pressing confirm, send a $method_form request to /books
            'confirmText' => 'Create', //Label confirm button as 'Create'
            'cancelAction' => '/' . self::DIRECTORY, //Upon pressing cancel, redirect user back to /books
            'cancelText' => 'Cancel' //Label cancel button as 'Cancel'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Book::create($this->validateBook($request)); //Validate request data before creating
        return redirect('/' . self::DIRECTORY); //Redirects to /books after successful creation
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view(self::DIRECTORY . '.show', [
            'title' => 'Show Book', //Form title
            'items' => $book->showableAttributes(), //Fetch showable attributes from searched book
            'readonly' => true, //Input fields are not editable
            'method_form' => 'HEAD', //Upon pressing confirm, send a HEAD request to $confirmAction
            'method' => '', //No custom method is required
            'confirmAction' => '/' . self::DIRECTORY . '/' . $book->id . '/edit', //Upon pressing confirm, send a $method_form request to /books/{{ $book->id }}/edit
            'confirmText' => 'Go to Edit', //Label confirm button as 'Go to Edit'
            'cancelAction' => '/' . self::DIRECTORY, //Upon pressing cancel, redirect user back to /books
            'cancelText' => 'Back to Books' //Label cancel button as 'Back to Books'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view(self::DIRECTORY . '.edit', [
            'title' => 'Edit Book', //Form title
            'author' => $book->author, //The current author of the book
            'items' => $book->editableAttributes(), //Fetch editable attributes from searched book
            'readonly' => false, //Input fields are editable
            'method_form' => 'POST', //Upon pressing confirm, send a POST request to $confirmAction
            'method' => method_field('PATCH'), //Use laravel custom method PATCH
            'confirmAction' => '/' . self::DIRECTORY . '/' . $book->id, //Upon pressing confirm, send a $method_form request to /books/{{ $book->id }}
            'confirmText' => 'Submit', //Label confirm button as 'Submit'
            'cancelAction' => '/' . self::DIRECTORY . '/' . $book->id, //Upon pressing cancel, redirect user back to /books/{{ $book->id }}
            'cancelText' => 'Cancel' //Label cancel button as 'Cancel'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $book->update($this->validateBook($request)); //Validate request data before updating
        return redirect('/' . self::DIRECTORY . '/' . $book->id); //Redirects to /books/{{ $book->id }} after successful update
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete(); //Delete the selected book
        return redirect('/' . self::DIRECTORY); //Redirects to /books after successful delete
    }

    /**
     * Validation of author data for create and update
     */
    public function validateBook($request)
    {
        /**
         * Combination of ['author_id', 'title'] has to be unique
         *
         * For example, the following records are NOT allowed :
         * id           author_id    title
         * 1            1            My Book Title
         * 2            1            My Book Title
         *
         * However, the following records are allowed :
         * id           author_id    title
         * 1            1            My Book Title
         * 2            2            My Book Title
         */ 

        /**
         * Validation for unique combination ['author_id', 'title']
         */

        $uniqueAuthorBook = Rule::unique('books')->where(function($query) use ($request) {
            return $query
                ->where('author_id', $request->author_id)
                ->where('title', $request->title);
        });

        /**
         * Custom error message for unique combination violation
         */

        $errorMessage = [
            'title.unique' => 'The author already has a book with that title.' 
        ];

        /**
         * Author must also exist within the authors table
         */

        $rules = [
            'author_id' => ['required', 'exists:authors,id'],
            'title' => ['required', 'min:5', $uniqueAuthorBook]
        ];

        return request()->validate($rules, $errorMessage);
    }

    public function exportToCsv($key)
    {
        if (array_key_exists($key, self::EXPORT_COLUMNS_KEY))
        {
            return Excel::download(new BooksExport(self::EXPORT_COLUMNS_KEY[$key]), 'books.csv');
        }
        else
        {
            abort(404);
        }
    }

    public function exportToXml($key)
    {
        if (array_key_exists($key, self::EXPORT_COLUMNS_KEY))
        {
            $exporter = new BooksExport(self::EXPORT_COLUMNS_KEY[$key]);
            $xml = $exporter->toXML();
            return response($xml)
                ->header('Content-Type', 'text/xml')
                ->header('Content-Disposition', 'attachment; filename="books.xml"');
        }
        else
        {
            abort(404);
        }
    }
}
