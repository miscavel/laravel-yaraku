<?php

namespace App\Exports;

use App\Author;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\XMLParser;

class AuthorsExport implements FromCollection, WithHeadings
{
	public function __construct($columns)
    {
        $this->columns = $columns;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Author::all($this->columns);
    }

    public function headings(): array
    {
        return $this->columns;
    }

    public function toXML()
    {
        $xmlParser = new XMLParser();
        return $xmlParser->parse($this->collection()->toArray(), 'authors', 'author');
    }
}
