<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\QueryException;
use App\Author;

class AuthorsTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    /** @test */
    public function create_author()
    {
        //User is able to create author by passing ['name'] as parameter
        
        //Author's attributes
        $param = ['name' => $this->faker->name()];
        
        //Create author with the given attributes
        Author::create($param);

        //Check if database has an entry with the given attributes
        $this->assertDatabaseHas('authors', $param);
    }

    /** @test */
    public function check_author_unique_on_creation()
    {
        //A test to verify that author's name is unique on creation

        //Author's attributes
        $param = ['name' => $this->faker->name()];

        //Create 1st author with the given attributes
        Author::create($param);
        try
        {
            //Create 2nd author with the same attributes
            Author::create($param);

            //If error not caught, means database fails in rejecting registration of existing author name
            $this->assertTrue(false);
        }
        catch (QueryException $exception)
        {
            //If error caught, means database successfully rejects registration of existing author name
            $this->assertTrue(true);
        }
    }

    /** @test */
    public function update_author()
    {
        //User is able to edit an author's name

        //Author's original attributes
        $param = ['name' => $this->faker->name()];

        //Author's attributes after update
        $param_edit = ['name' => 'Edited Author Name'];
        
        //Create original author
        $author = Author::create($param);

        //Update author's attributes into the new values
        $author->update($param_edit);

        //Check if database has an entry with the new attributes
        $this->assertDatabaseHas('authors', $param_edit);
    }

    /** @test */
    public function check_author_unique_on_update()
    {
        //A test to verify that author's name is unique on update

        //1st author's attributes
        $param = ['name' => $this->faker->name()];

        //2nd author's attributes
        $param_2 = ['name' => '2nd Author'];

        //Create 1st author
        Author::create($param);

        //Create 2nd author
        $author = Author::create($param_2);

        try
        {
            //Update 2nd author's name into the 1st author's name
            $author->update($param);

            //If error not caught, means database fails in rejecting update into an existing author name
            $this->assertTrue(false);
        }
        catch (QueryException $exception)
        {
            //If error caught, means database successfully rejects update into an existing author name
            $this->assertTrue(true);
        }
    }

    /** @test */
    public function delete_author()
    {
        //A test to check that a delete call removes that author's entry from the database

        //Author's attributes
        $param = ['name' => $this->faker->name()];
        
        //Create author with the given attributes
        $author = Author::create($param);

        //Delete author
        $author->delete();

        //Verify that the author's entry is not found in the database
        $this->assertDatabaseMissing('authors', $param);
    }
}
