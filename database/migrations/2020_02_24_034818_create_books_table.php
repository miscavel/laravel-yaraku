<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('author_id');
            $table->string('title');
            $table->timestamps();

            /**
             * Combination of ['author_id', 'title'] has to be unique
             *
             * For example, the following records are NOT allowed :
             * id           author_id    title
             * 1            1            My Book Title
             * 2            1            My Book Title
             *
             * However, the following records are allowed :
             * id           author_id    title
             * 1            1            My Book Title
             * 2            2            My Book Title
             */ 
            $table->unique(['author_id', 'title']);

            $table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
