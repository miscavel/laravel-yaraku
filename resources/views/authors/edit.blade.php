@extends('authors.layout')

@section('content')
	<div class="box">
		
		{{--
		 *
		 * Displays authors array passed through as $items with form-layout
		 *
		 --}}

		@include('form-layout')
	</div>
@endsection