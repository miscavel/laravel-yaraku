<script type="text/javascript">
	
	/**
	 * Used to show dropdown
	 */
	function enableDropdown(element)
	{
		$(element).addClass("is-active");
	}

	/**
	 * Used to update input element value based on the selected dropdown value
	 */
	function updateInput(element, value)
	{
		$(element).val(value);
	}

	/**
	 * AJAX call to fetch a list of authors that contain the given name
	 * ajaxCounter is used mainly to prevent multiple feedbacks when AJAX is called multiple times
	 */
	var ajaxCounter = 0;
	function fetchAuthors(name)
	{
		incrementAjaxCounter();
		var currentCounter = ajaxCounter;

		/**
		 * Shows loading bar and empties dropdown list at the start of AJAX call
		 */
		$("#dropdown-control").addClass("is-loading");
		$("#dropdown-content").empty();

		if (!(name === ''))
		{
			$.get('/authors/fetch/' + name, function(data) {
				if (ajaxCounter == currentCounter)
				{
					/**
					 * If it is the last AJAX call, populate the dropdown list with the fetched authors data
					 */
					var json = JSON.parse(data);
					updateAuthors(json);

					/**
					 * Removes loading bar
					 */
					$("#dropdown-control").removeClass("is-loading");
				}
			});
		}
		else
		{
			/**
			 * Removes loading bar
			 */
			$("#dropdown-control").removeClass("is-loading");
		}
	}

	/**
	 * ajaxCounter increment formula, resets back to 0 after every 1000 increments
	 */
	function incrementAjaxCounter()
	{
		ajaxCounter = (ajaxCounter + 1) % 1000;
	}

	/**
	 * Used to reset input value to the last valid author selected (or blank if no valid author has been selected before)
	 *
	 * For example, originally the input value was an existing author called 'John'
	 *
	 * If user searches for 'Jack' and no result is found, upon clicking outside of the input box the input value 
	 * would be reset back to 'John'
	 *
	 * Is related to rememberName and rememberID variables set at the bottom of the script
	 */
	function updateAuthorsValue(nameElement, name, idElement, id)
	{
		updateInput(nameElement, name);
		updateInput(idElement, id);
		rememberAuthor(name, id);
	}

	/**
	 * Generates dropdown items based on the AJAX call on authors fetch API
	 * 
	 * Each item has an anchor tag that sets the hidden author_id to its corresponding value
	 *
	 * For example, choosing 'John' would set the author_id to that of John's id
	 */
	function updateAuthors(json)
	{
		json.forEach(function(item) {
			var element = "<a class=\"dropdown-item\" onclick=\"updateAuthorsValue('#dropdown-trigger', '" + item['name'] + "', '#author-id', '" + item['id'] + "')\">" + item['name'] + "</a>";
			$("#dropdown-content").append(element);
		});
	}

	/**
	 * Register events on window load
	 */
	$(document).ready(function () 
	{
		/**
		 * Clicking on anywhere outside of input box would make the dropdown disappear
		 */
		$(window).click(function() {
			$("#dropdown").removeClass("is-active");
			resetAuthor();
		});

		/**
		 * Update dropdown suggestion everytime the user types on the input box (AJAX fetch authors API)
		 *
		 * At the same time, displays the dropdown list
		 */
	    $("#dropdown-trigger").keyup(function () {
			enableDropdown("#dropdown");
			fetchAuthors($("#dropdown-trigger").val());
		});

	    /**
		 * Shows the dropdown list upon clicking on the input box (however it does not call the AJAX fetch authors API)
		 */
		$("#dropdown-trigger").click(function () {
			enableDropdown("#dropdown");
			event.stopPropagation();
		});

		/**
		 * Prevents clicking on input box from hiding the dropdown list
		 */
		$(".dropdown-item").click(function () {
			event.stopPropagation();
		})
	});
</script>
<div class="dropdown field" id="dropdown">
	<div>
		<label class="label" for="author">Author</label>
    	<p class="control" id="dropdown-control">
    		<input type="hidden" name="author_id" id="author-id" value="{{ ($errors->has('author_id') || $errors->has('title')) ? old('author_id') : (isset($author) ? $author->id : '-1') }}"/>
        	<input class="input {{ $errors->has('author_id') ? 'is-danger' : '' }}" type="text" placeholder="Search Author..." name="author_name" id="dropdown-trigger" autocomplete="off" value="{{ ($errors->has('author_id') || $errors->has('title')) ? old('author_name') : (isset($author) ? $author->name : '') }}"/>
    	</p>
    </div>
	<div class="dropdown-menu" role="menu">
        <div class="dropdown-content" id="dropdown-content">

        </div>
    </div>
</div>

<script type="text/javascript">

	/**
	 * The variables and functions used for the 'remember' function of input box
	 *
	 * Work together with updateAuthorsValue
	 */
	 
	var rememberName = $("#dropdown-trigger").val();
	var rememberID = $("#author-id").val();

	function rememberAuthor(name, id)
	{
		rememberName = name;
		rememberID = id;
	}

	function resetAuthor()
	{
		$("#dropdown-trigger").val(rememberName);
		$("#author-id").val(rememberID);
	}
</script>