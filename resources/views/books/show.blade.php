@extends('books.layout')

@section('content')
	<div class="box">

		{{--
		 *
		 * Displays books array passed through as $items with form-layout
		 *
		 --}}

		@include('form-layout')
	</div>
@endsection