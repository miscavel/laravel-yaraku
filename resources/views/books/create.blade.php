@extends('books.layout')


{{--
 *
 * Adds another field to the form layout
 *
 --}}
@section('additional-input')
	
	{{--
	 *
	 * Adds an author-searcher field that suggests registered authors that contain the typed-in name
	 *
	 --}}

	@include('authors.searcher')
@endsection

@section('content')
	<div class="box">

		{{--
		 *
		 * Displays books array passed through as $items with form-layout
		 *
		 --}}

		@include('form-layout')
	</div>
@endsection