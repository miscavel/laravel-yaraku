<!DOCTYPE html>
<html>
<head>
	<title>Laravel Yaraku</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.8.0/css/bulma.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<style>
		.navitem-active
		{
			background-color : #fafafa;
			color : #3273dc;
		}
	</style>
	@yield('header')
</head>
<body class="has-navbar-fixed-top">
	@include('navbar')
	<div class="container">
		@yield('content')
	</div>
</body>
</html>