
{{-- 
 *
 * Only render table when there are items to be displayed 
 *
 --}}
 
@if (isset($items) && count($items) > 0)

	{{-- 
	 *
	 * margin:auto to center table
	 *
	 --}}

	<table class="table is-bordered is-striped is-hoverable is-fullwidth is-narrow" style="margin: auto;">
		<thead>
			<tr>

				{{-- 
			     *
				 * Loop through item keys for table header
				 *
				 * Only render columns listed in App\Author::showableAttributes()
				 *
				 --}}

				@foreach ($items[0]->showableAttributes() as $key => $value)
					<th>{{ ucfirst($key) }}</th>
				@endforeach

				{{--
				 *
				 * Loop through included options (view / edit / delete) and create the corresponding header
				 *
				 --}}

				@foreach ($options as $option)
					<th class="has-text-centered">{{ $option['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>

			{{--
			 * 
			 * Loop through all items for table content
			 *
			 * Only render columns listed in App\Author::showableAttributes()
			 *
			 --}}

			@foreach ($items as $item)
				<tr>
					@foreach ($item->showableAttributes() as $key => $value)
						<td>
							{{ $value }}
						</td>
					@endforeach

					{{--
					 *
					 * Loop through included options (view / edit / delete) and create the corresponding element
					 *
					 * link is used to enable 'open in new tab', whilst button is used to submit forms
					 * (e.g. delete request)
					 *
					 --}}

					@foreach ($options as $option)
						<td class="has-text-centered">
							@if ($option['type'] === 'link')
								<a style="color: inherit;" href="{{ $option['action_prefix'] . '/' . $item->id . $option['action_suffix'] }}">
									<i class="fa {{ $option['icon'] }}" aria-hidden="true"></i>
								</a>
							@else
								<form method="{{ $option['method_form'] }}" action="{{ $option['action_prefix'] . '/' . $item->id . $option['action_suffix'] }}">
								@if ($option['method_form'] === 'POST')
									@csrf
								@endif
								{{ $option['method'] }}
								<button>
									<i class="fa {{ $option['icon'] }}" aria-hidden="true"></i>
								</button>
							@endif
							</form>
						</td>
					@endforeach
				</tr>
			@endforeach
		</tbody>
	</table>
@endif